# ComCms

#### 介绍
贵州情报板文件更新

#### 软件架构
贵州情报板软件更新




#### 使用说明
ComCms dll文件
``` c++
//情报板厂家类型对应编号
#define CMS_TONGXIN				1201	//同鑫
#define CMS_CHENGLUO				1202	//同鑫 成洛大道
#define CMS_XINGHENG				1203	//同鑫 河北邢衡
#define CMS_SANSI				1204	//三思
#define CMS_DIANMING				1205	//电明 play01(长天智远)
#define CMS_DIANMING_OLD			1206	//电明老版本
#define CMS_DIANMING_Linux			1207	//电明linux
#define CMS_DIANMING_Linux_Single	        1208	//电明linux 单色 (单色的拼装报文与其他两个不同)
#define CMS_DIANMING_Linux_PLC		        1209	//电明linux PLC 解析函数有点区别
#define CMS_XIANKE				1210	//显科
#define CMS_YINGSHA				1211	//英沙
#define CMS_ZHONGHAI				1212	//中海
#define CMS_DINGEN				1213	//鼎恩
#define CMS_FENGHAI				1214	//丰海
#define CMS_SANSI_SP				1215	//三思带间距0
#define CMS_JINXIAO				1216	//金晓
#define CMS_JINXIAO_DONGNEI			1217	//金晓洞内
#define CMS_FENGLEI			        1218	//沣雷
#define CMS_HANWEI			        1219	//汉威光电
#define CMS_SANSI_V6				1223	//汶马路三思v6协议 
#define CMS_GDBT				1225    //汶马路光电比特
#define CMS_NUOWA				1226    //诺瓦
#define CMS_DIANMING_OLD_CT			1305	//电明老版本 play01(长天智远)
#define CMS_SANSI_SINGLE			1304	//三思单色
#define CMS_DIANMING_YELLO_SINGLE	        1306	//电明 play01(长天智远、黄色单色)
#define CMS_XIANKE_YELLO_SINGLE		        1307	//显科（单黄色）
#define CMS_ZHONGHAI_SP				1308	//中海（三思协议）
#define CMS_SANSI_SP1			        1309	//三思协议 playlist
#define CMS_TONGXIN_ST			        1310	//同鑫标准协议
```

